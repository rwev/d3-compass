import { faker } from '@faker-js/faker'
import * as R from 'ramda'
import * as d3 from 'd3'

// correlated time and variance outcome
// standard deviation for each variable
// record previous occurance value, and time since last occurance, and compute neew value (prev) + (sigma * time)

// TODO check out d3.random
// https://observablehq.com/@d3/d3-random

const START_DATE = new Date(Date.now())
START_DATE.setMonth(START_DATE.getMonth() - 3)

function shiftDays(date: Date, days: number): Date {
  let newDate = new Date(date)
  newDate.setDate(newDate.getDate() + days)
  return newDate
}

const dayShiftGenerator = (): number => d3.randomExponential(1 / 5)()
const valueShiftGenerator = (days: number): number => d3.randomNormal(0, 0.25)() * Math.sqrt(days / 365)

export const generateTimeSeries = (count: number): [any, any][] => {
  let date = START_DATE
  let value = 100
  return R.pipe(
    R.range(0),
    R.map((): any => {

      const dayShift = dayShiftGenerator()
      value = value * (1 + valueShiftGenerator(dayShift))
      date = faker.date.between(date, shiftDays(date, dayShift))
      return { date, value }
    }),
    R.sortBy(R.prop('date')),
    R.map((point: { date: Date; value: number }) => ([point.date, point.value]))
  )(count) as any
}


import * as d3 from 'd3'

// TODO make typescript

/*
Derived from https://observablehq.com/@d3/line-with-tooltip
Copyright 2021 Observable, Inc.
Released under the ISC license.

@rwev 
- added classes to style separately with scss
- added update callback
- simplified options (e.g. removed stylization parameters)
*/

export function LineChart(
  data,
  {
    xGetter = ([dX, _]) => dX,
    yGetter = ([, dY]) => dY,
    isDefined = (d, _i) => !isNaN(xGetter(d)) && !isNaN(yGetter(d)),
    tooltipTextGetter = (d, _i) => {
      const yStr = yGetter(d).toFixed(2)
      const xStr = xGetter(d).toLocaleDateString()
      return `${xStr}\n${yStr}`
    },

    curve = d3.curveLinear, // method of interpolation between points
    margin = 50, // margin
    width = 750, // outer width, in pixels
    height = 375, // outer height, in pixels
    tickSpacing = 100,
    xScaleType = d3.scaleUtc, // type of x-scale
    xDomainGetter = d3.extent, // [xmin, xmax]
    yDomainGetter = d3.extent,
    yScaleType = d3.scaleLinear, // type of y-scale
    transitionDuration = 500
  } = {}) {

  const xRange = [margin, width - margin]
  const yRange = [height - margin, margin]

  function pointerleft() {
    tooltip.style('display', 'none')
    svg.dispatch('input', { bubbles: true })
  }

  const svg = d3.create('svg')
    .attr('class', 'line-chart-with-tooltip')
    .attr('width', width)
    .attr('height', height)
    .attr('viewBox', [0, 0, width, height])
    .attr('style', 'max-width: 100%; height: auto; height: intrinsic;')
    .style('overflow', 'visible')
    .on('pointerleave', pointerleft)
    .on('touchstart', event => event.preventDefault())

  const areaPath = svg.append('path')
    .attr('class', 'area')

  const gXAxis = svg.append('g')
    .attr('class', 'axis x')
    .attr('transform', `translate(0,${height - margin})`)

  const gYAxis = svg.append('g')
    .attr('transform', `translate(${margin},0)`)
    .attr('class', 'axis y')

  const linePath = svg.append('path')
    .attr('class', 'line')

  const tooltip = svg.append('g')
    .on('pointerenter pointermove', (e) => e.stopPropagation())

  const updateData = (_data) => {

    // Compute values.
    const X = d3.map(_data, xGetter)
    const Y = d3.map(_data, yGetter)

    const XScale = xScaleType(xDomainGetter(X), xRange)
    const YScale = yScaleType(yDomainGetter(Y), yRange)

    const XAxis = d3.axisBottom(XScale).ticks(width / tickSpacing)
    const YAxis = d3.axisLeft(YScale).ticks(height / tickSpacing)

    const line = d3.line()
      .defined(d => isDefined(d))
      .curve(curve)
      .x(d => XScale(xGetter(d)))
      .y(d => YScale(yGetter(d)))

    const area = d3.area()
      .x(d => XScale(xGetter(d)))
      .y1(d => YScale(yGetter(d)))
      .y0(height - margin)

    areaPath.data([_data]).transition().duration(transitionDuration).attr('d', area)
    linePath.data([_data]).transition().duration(transitionDuration).attr('d', line)
    gXAxis.transition().duration(transitionDuration).call(XAxis)
    gYAxis.transition().duration(transitionDuration).call(YAxis)
    gYAxis
      .call(g => g.selectAll('.grid-line').remove())
      .call(g => g.selectAll('.tick line').clone()
        .attr('class', 'grid-line')
        .attr('x2', width - 2 * margin)
      )
      .call(g => g.append('text')
        .attr('x', -margin)
        .attr('y', 10)
      )

    svg.on('pointerenter pointermove', (event) => {

      const dataIndex = d3.bisectCenter(X, XScale.invert(d3.pointer(event)[0]))
      const dataPoint = _data[dataIndex]

      tooltip.style('display', null)
      tooltip.attr('transform', `translate(${XScale(X[dataIndex])},${YScale(Y[dataIndex])})`)

      const path = tooltip.selectAll('path')
        .data([,])
        .join('path')
        .attr('class', 'line-chart-tooltip')

      const text = tooltip.selectAll('text')
        .data([,])
        .join('text')
        .attr('class', 'line-chart-tooltip-text')
        .call(tooltipText => tooltipText
          .selectAll('tspan')
          .data(`${tooltipTextGetter(dataPoint, dataIndex)}`.split(/\n/))
          .join('tspan')
          .attr('x', 0)
          .attr('y', (_, i) => `${i * 1.1}em`)
          .text(d => d)
        )

      const { x, y, width: w, height: h } = text.node().getBBox()
      text.attr('transform', `translate(${-w / 2},${15 - y})`)
      path.attr('d', `M${-w / 2 - 10},5H-5l5,-5l5,5H${w / 2 + 10}v${h + 20}h-${w + 20}z`)
      svg.property('value', _data[dataIndex]).dispatch('input', { bubbles: true })
    })
  }

  // initial data 
  updateData(data)

  return Object.assign(svg, { updateData })
}

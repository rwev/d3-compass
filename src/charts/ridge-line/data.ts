import * as R from 'ramda'
import * as d3 from 'd3'

const r: () => number = d3.randomNormal(0, 1)

export const normalDistributionSeries = (samples: number): any => {

  const length = 50
  let events = R.pipe(
    R.range(0),
    R.map(r),
    R.sort(R.subtract)
  )(samples) as number[]

  const globalMin = Math.min(...events)
  const globalMax = Math.max(...events)

  const increment = (globalMax - globalMin) / length

  const buckets: { min: number; max: number }[] = R.pipe(
    R.range(0),
    R.map((i: number): any => {
      let min = globalMin + increment * i
      const max = globalMin + increment * (i + 1)
      return { min, max }
    }
    )
  )(length)

  const bucketCounts = R.pipe(
    R.map(({ min, max }: any) => R.count((val: number): boolean => val > min && val < max, events))
  )(buckets)

  return bucketCounts
}

export const generateSeries = (seriesCount: number, seriesLength: number): number[][] => {

  const rootSeries: number[] = []

  let value = 0
  R.times(
    () => {
      value = value + r()
      rootSeries.push(value)
    }
  )(seriesLength)

  return R.pipe(
    R.range(0),
    R.map(() => ([])),
    R.map((array: number[]): number[] => {
      R.times(
        (i: number): void => {
          array.push((rootSeries[i] as number) + r())
        }
      )(seriesLength)
      return array
    }),
  )(seriesCount) as number[][]

}
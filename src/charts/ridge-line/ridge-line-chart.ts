import * as d3 from 'd3'

// TODO improve typing
/*
Derived (roughly) from https://observablehq.com/@mbostock/psr-b1919-21
- made parameterizable
- style with classes
*/

export const Ridgeline = (
  data: number[][] = [[]],
  {
    width = 750,
    height = 500,
    marginX = 50,
    marginY = 50,
    overlap = 5
  } = {}): any => {

  const y = d3.scalePoint<number>()
    .domain(data.map((_d: number[], i: number): number => i)) // indices of each serie
    .range([marginY, height - marginY])

  const x = d3.scaleLinear()
    .domain([0, (data[0]?.length ?? 0) - 1])
    .range([marginX, width - marginX])

  const z = d3.scaleLinear()
    .domain([
      // compute global min, maxes (from all series)
      d3.min(data, d => d3.min(d)),
      d3.max(data, d => d3.max(d))
    ] as any) // FIXME
    .range([0, -overlap * y.step()])

  const area = d3.area()
    .defined((d: any): boolean => !isNaN(d))
    .x((_d, i) => x(i))
    .y0(0)
    .y1(z as any)

  const line = area.lineY1()

  const svg = d3.create('svg')
    .attr('class', 'ridgeline')
    .attr('width', width)
    .attr('height', height)
    .attr('viewBox', [0, 0, width, height])

  const serie = svg.append('g')
    .selectAll('g')
    .data(data)
    .join('g')
    .attr('transform', (_d, i) => `translate(0,${y(i as any) as any + 1})`)

  serie.append('path')
    .attr('class', 'area')
    .attr('d', area as any)

  serie.append('path')
    .attr('class', 'line')
    .attr('d', line as any)

  // "call" variable syntax allows reuse of function chain
  const xAxis = (g: any): any => g
    .attr('transform', `translate(0,${height - marginY})`)
    .call(d3.axisBottom(x.copy()).ticks(width / 100))
    // .call((_g: any): any => _g.select('.domain').remove()) // remove X-axis line
    .call((_g: any): any => _g.select('.tick:first-of-type text').append('tspan').attr('x', 10).text(' ms'))

  svg.append('g')
    .call(xAxis)

  svg.append('g')
    .call((g: any): any => g
      .attr('transform', `translate(${marginX},0)`)
      .call(d3.axisLeft(y.copy()))
    )

  svg.append('g')
    .attr('transform', `translate(${width - marginX}, ${height - marginY})`)
    .call(d3.axisRight(z.copy()).ticks(4))

  return svg.node()
}

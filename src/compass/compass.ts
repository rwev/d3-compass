import * as d3 from 'd3'
import { config, DialMark, dialCardinals, dialDegrees, dialMarks } from './config'

/**
 * TODO: 
 * - declination?
 * - deviceorientation for real usage
 * - provide function to instantion in specified element
 */


export const d3Compass = (): [SVGElement, (x: number) => void] => {

  let svg = d3.create("svg")
    .attr("width", config.size)
    .attr("height", config.size)
    .attr("class", "compass")



  /* 
  needle.append("line")
    .attr("class", "needle-shaft")
    .attr("x1", config.centerPoint().x)
    .attr("y1", config.centerPoint().y)
    .attr("x2", config.centerPoint().x)
    .attr("y2", config.centerPoint().y - config.needleRadius()) // initial position 360deg (N)
   */

  svg.append("circle")
    .attr("class", "circle")
    .attr("cx", config.centerPoint().x)
    .attr("cy", config.centerPoint().y)
    .attr("r", config.tickRadius())

  let needle = svg.append("g").attr("class", "needle")

  let triangle = d3.symbol()
    .type(d3.symbolTriangle)
    .size(config.triangleSize())

  needle.append("path")
    .attr("class", "needle-point")
    .attr("d", triangle)
    .attr("x", config.centerPoint().x)
    .attr("y", config.centerPoint().y)
    .attr("transform", `translate(${config.centerPoint().x}, ${config.centerPoint().y - config.needleRadius()})`);

  let heading = svg.append('g').attr("class", "heading")

  let headingDegrees = heading.append("text").attr("class", "heading-degrees")
    .attr("x", config.centerPoint().x)
    .attr("y", config.centerPoint().y + 2 * config.headingOffset())
    .style('text-anchor', 'middle')

  let headingCardinal = heading.append("text").attr("class", "heading-cardinal")
    .attr("x", config.centerPoint().x)
    .attr("y", config.centerPoint().y - config.headingOffset())
    .style('text-anchor', 'middle')
    .text(`${degreesToCompassCardinalDirection(360)}`)


  let dial = svg.append("g").attr("class", "dial")

  dial
    .append("g")
    .attr("class", "dial-degrees")
    .selectAll("text")
    .data(dialDegrees)
    .enter()
    .append("text")
    .attr("x", (b: DialMark) => (config.bearingRadius() * b.location.x) + config.centerPoint().x)
    .attr("y", (b: DialMark) => (config.bearingRadius() * b.location.y) + config.centerPoint().y)
    .style('text-anchor', 'middle') // center text above location point
    // .attr("rotate", (b: DialMark) => b.degrees / 2) // this rotates each text anchor
    // NOTE: must specify position to rotate arounnd, otherwise rotation center defaults to SVG origin 0,0
    .attr("transform", (b: DialMark) => `rotate(${b.degrees}, ${(config.bearingRadius() * b.location.x) + config.centerPoint().x}, ${(config.bearingRadius() * b.location.y) + config.centerPoint().y})`)
    .text((b: DialMark) => b.degrees)


  dial
    .append("g")
    .attr("class", "dial-ticks")
    .selectAll("line")
    .data(dialMarks)
    .enter()
    .append("line")
    .attr("x1", (b: DialMark) => (config.tickRadius() * b.location.x) + config.centerPoint().x)
    .attr("y1", (b: DialMark) => (config.tickRadius() * b.location.y) + config.centerPoint().y)
    .attr("x2", (b: DialMark) => ((config.tickRadius() - ((b.degrees % 10) === 0 ? config.tickLength() * 2 : config.tickLength())) * b.location.x) + config.centerPoint().x)
    .attr("y2", (b: DialMark) => ((config.tickRadius() - ((b.degrees % 10) === 0 ? config.tickLength() * 2 : config.tickLength())) * b.location.y) + config.centerPoint().y)


  dial
    .append("g")
    .attr("class", "dial-cardinals")
    .selectAll("line")
    .data(dialCardinals)
    .enter()
    .append("text")
    .attr("x", (b: DialMark) => (config.cardinalRadius() * b.location.x) + config.centerPoint().x)
    .attr("y", (b: DialMark) => (config.cardinalRadius() * b.location.y) + config.centerPoint().y)
    .attr("class", (b: DialMark) => (b.degrees % 90) === 0 ? 'dial-cardinal-primary' : '')
    .style('text-anchor', 'middle') // center text above location point
    // .attr("rotate", (b: DialMark) => b.degrees / 2) // this rotates each text anchor
    // NOTE: must specify position to rotate arounnd, otherwise rotation center defaults to SVG origin 0,0
    .attr("transform", (b: DialMark) => `rotate(${b.degrees}, ${(config.cardinalRadius() * b.location.x) + config.centerPoint().x}, ${(config.cardinalRadius() * b.location.y) + config.centerPoint().y})`)
    .text((b: DialMark) => degreesToCompassCardinalDirection(b.degrees))


  function degreesToCompassCardinalDirection(degree: number): string {
    const val = Math.floor((degree / 45) + 0.5);
    const cardinals: string[] = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
    return cardinals[(val % 8)] as string
  }

  const update = (degrees: number): void => {
    const currentHeading: number = +headingDegrees.text() ?? 0

    /* 
    // TODO update rotation transition to take shortest path, either clockwise or ccw

    function clockwise(last: number, current: number): boolean {
      if (current > last) {
        return (current - last) < 180
      } else {
        return (last - current) > 180
      }
    }
    const updateClockwise: boolean = clockwise(currentHeading, degrees)
    console.log(`${currentHeading} -> ${degrees}, ${updateClockwise ? `clockwise` : 'counter'}`)
 */

    function interpolateTransform() {
      return d3.interpolateString(
        // transition between current transform position and the new one
        needle.attr("transform"),
        `rotate(${degrees}, ${config.centerPoint().x}, ${config.centerPoint().y})`
      )
    }

    needle.transition().duration(config.transitionMs).attrTween("transform", interpolateTransform)
    // this achieves animation, but the end of needle moves (doesn't stay at center)
    // needle.transition().duration(500).attr("transform", `rotate(${Math.random() * 360}, ${config.centerPoint().x}, ${config.centerPoint().y})`)


    headingCardinal.text(`${degreesToCompassCardinalDirection(degrees)}`)
    headingDegrees.transition()
      .duration(config.transitionMs)
      .textTween(() => (prog: number): string => {
        return `${Math.round((degrees - currentHeading) * prog + currentHeading) % 360}`
      }
      )
  }

  update(0)

  return [svg.node() as SVGElement, update]
}



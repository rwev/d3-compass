import * as R from 'ramda'

export const config = {
  size: 500,
  transitionMs: 1000,
  padding: () => (config.size / 12),
  tickLength: () => (config.padding() / 4),
  radius: () => (config.size / 2) - config.padding(),
  tickRadius: () => config.radius(),
  bearingRadius: () => config.radius() - config.padding(),
  cardinalRadius: () => config.radius() - 2 * config.padding(),
  needleRadius: () => config.radius() - 2 * config.padding(),
  headingOffset: () => config.padding(),
  triangleSize: () => config.size * 2,
  centerPoint: (): Point => ({
    x: config.size / 2,
    y: config.size / 2
  })
}

const filterDialRange = (range: DialMark[], every: number): DialMark[] => {
  return R.filter((m: DialMark) => (m.degrees % every) === 0, range)
}

export const dialMarks: DialMark[] = R.pipe(
  R.range(0),
  R.map((degrees: number): DialMark => {
    const radians = (degrees - 90) * Math.PI / 180 // rotate 90 degrees to vertical
    return {
      degrees,
      radians,
      location: {
        x: Math.cos(radians),
        y: Math.sin(radians)
      }
    }
  }),
)(360)

export const dialDegrees = filterDialRange(dialMarks, 20)
export const dialCardinals = filterDialRange(dialMarks, 45)

export type Point = {
  x: number,
  y: number
}
export type DialMark = {
  degrees: number
  radians: number
  location: Point
}




  /*
  
  let debugText = svg
    .append("text")
    .attr("x", config.centerPoint().x)
    .attr("y", config.centerPoint().y)
    .text("debug")
  
  // TODO listen to orientation to make compass 'functional'
  Promise.all([navigator.permissions.query({ name: "accelerometer" } as any),
  navigator.permissions.query({ name: "gyroscope" } as any)])
    .then(results => {
      if (results.every(result => result.state === "granted")) {
        const options = { frequency: 6, referenceFrame: 'device' };
  
        // @ts-ignore
        const sensor = new (RelativeOrientationSensor as any)(options);
  
        sensor.addEventListener('reading', (event: any) => debugText.text(JSON.stringify(event)))
        sensor.addEventListener('error', (event: any) => debugText.text(JSON.stringify(event)))
        sensor.start();
      } else {
        console.log("No permissions to use RelativeOrientationSensor.");
      }
    });
  
   */

  // https://dev.to/trekhleb/gyro-web-accessing-the-device-orientation-in-javascript-2492

  /* 
  if (!window.DeviceOrientationEvent) {
    window.addEventListener('deviceorientation', (event: DeviceOrientationEvent) => {
      debugText.text(JSON.stringify(event))
    });
  }
  
   */
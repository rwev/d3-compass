import './styles.scss'

import * as R from 'ramda'
import { d3Compass } from './compass/compass'
import { d3Slider } from './slider/slider'
import { createScoreDial } from './score-dial/score-dial'
import { createStatusBar } from './status-bar/status-bar'
import { LineChart } from './charts/line-tooltip/line-chart-with-tooltip'
import { Ridgeline } from './charts/ridge-line/ridge-line-chart'
import { generateTimeSeries } from './charts/line-tooltip/data'
import { normalDistributionSeries } from './charts/ridge-line/data'

document.body.append(
  Ridgeline(
    R.pipe(
      R.range(0),
      R.map((i) => normalDistributionSeries(1000 * (i + 1))),
    )(20))
)

const chart = LineChart(
  generateTimeSeries(100)
) as any

document.body.append(
  chart.node()
)

chart.updateData(generateTimeSeries(100))

const compassContainer = document.createElement('div')
compassContainer.classList.add('compass-container')

let [compassSvg, updateCompassDirection] = d3Compass()

compassContainer.appendChild(compassSvg)
compassContainer.appendChild(d3Slider(updateCompassDirection))

document.body.append(compassContainer)

const statusContainer = document.createElement('div')
statusContainer.classList.add('status-container')

const [dialSvg, dialUpdateFn] = createScoreDial()
const [statusBarSvg, successBarUpdateFn] = createStatusBar()

statusContainer.appendChild(dialSvg)
statusContainer.appendChild(statusBarSvg)

document.body.append(statusContainer)

const random = (upper: number): number => Math.random() * upper

  ;[dialUpdateFn, successBarUpdateFn].map((fn: any) => fn(random(100)))

R.pipe(
  R.times(createStatusBar),
  R.tap(
    R.map(
      ([svg, updateFn]: [any, (...args: any[]) => void]) => {
        statusContainer.appendChild(svg)
        updateFn(0)
        setTimeout(() => updateFn(random(100)), random(500))
      })
  ),

)(5)


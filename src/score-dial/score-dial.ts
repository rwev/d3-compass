import * as d3 from 'd3'

const config = {
  size: 250,
  transitionMs: 1000,
  arcBufferDeg: 1,

  center: () => config.size / 2,
  translateCenter: () => `translate(${config.center()},${config.center()})`,
  fontSize: () => 40 * (config.size / 250), // size 250 - font-size 40

  arcWidth: () => config.padding() / 2,
  padding: () => config.size / 12,

  activeRadiusOuter: () => config.center() /* - config.padding() */,
  activeRadiusInner: () => config.circleRadiusInner(),

  inactiveRadiusOuter: () => config.activeRadiusOuter() - config.arcWidth(),
  inactiveRadiusInner: () => config.activeRadiusOuter() - 2 * config.arcWidth(),

  circleRadiusOuter: () => config.inactiveRadiusInner() - config.arcWidth(),
  circleRadiusInner: () => config.circleRadiusOuter() - config.arcWidth(),
}

export const createScoreDial = (): [any, (...args: any[]) => void] => {

  const scoreDial = d3.create('svg').attr('width', config.size).attr('height', config.size).attr('class', 'score-dial')

  const rad = (deg: number): number => deg * (Math.PI / 180)

  const ring = scoreDial
    .append('path')
    .attr(
      'd',
      d3
        .arc()
        .innerRadius(config.circleRadiusInner())
        .outerRadius(config.circleRadiusOuter())
        .startAngle(rad(0))
        .endAngle(rad(360)) as any,
    )
    .attr('fill', 'unset')
    .attr('transform', config.translateCenter())

  const text = scoreDial
    .append('text')
    .attr('fill', 'white')
    .attr('class', 'score-dial-text')
    .style('font-size', config.fontSize())
    .style('font-weight', 700)
    .style('text-anchor', 'middle')
    .attr('x', config.center())
    .attr('y', config.center() + config.padding() / 2)

  const makeActiveArc = (startAngle: number, endAngle: number, active: boolean = false): any => d3
    .arc()
    .cornerRadius(5)
    .innerRadius(active ? config.activeRadiusInner() : config.inactiveRadiusInner())
    .outerRadius(active ? config.activeRadiusOuter() : config.inactiveRadiusOuter())
    .startAngle(rad(startAngle + config.arcBufferDeg))
    .endAngle(rad(endAngle - config.arcBufferDeg))

  const arcLenDeg = 180 / 3.0

  const redArcPath = scoreDial.append('path').attr('class', 'danger').attr('transform', config.translateCenter())
  const yellowArcPath = scoreDial.append('path').attr('class', 'warn').attr('transform', config.translateCenter())
  const greenArcPath = scoreDial.append('path').attr('class', 'success').attr('transform', config.translateCenter())

  const redArcPathData = (active?: boolean): any => makeActiveArc(270 - config.arcBufferDeg, 270 + arcLenDeg, active ?? false)
  const yellowArcPathData = (active?: boolean): any => makeActiveArc(270 + arcLenDeg, 270 + 2 * arcLenDeg, active ?? false)
  const greenArcPathData = (active?: boolean): any =>
    makeActiveArc(270 + 2 * arcLenDeg, 270 + 3 * arcLenDeg + config.arcBufferDeg, active ?? false)

  const setScoreDial = (score: number): void => {

    // reset all arcs
    redArcPath.transition().duration(config.transitionMs).attr('d', redArcPathData())
    yellowArcPath.transition().duration(config.transitionMs).attr('d', yellowArcPathData())
    greenArcPath.transition().duration(config.transitionMs).attr('d', greenArcPathData())

    if (score <= 33) {
      redArcPath.transition().duration(config.transitionMs).attr('d', redArcPathData(true))
      ring.transition().duration(config.transitionMs).attr('class', 'danger')
    } else if (score <= 66) {
      yellowArcPath.transition().duration(config.transitionMs).attr('d', yellowArcPathData(true))
      ring.transition().duration(config.transitionMs).attr('class', 'warn')
    } else {
      greenArcPath.transition().duration(config.transitionMs).attr('d', greenArcPathData(true))
      ring.transition().duration(config.transitionMs).attr('class', 'success')
    }
    text
      .transition()
      .duration(config.transitionMs)
      .textTween(() => (prog: number): string => `${(prog * score).toFixed(1)}`)
  }

  return [scoreDial.node(), setScoreDial]

}

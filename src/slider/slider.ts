import * as d3 from 'd3'

export const d3Slider = (callback: (val: number) => any): SVGElement => {

  let svg = d3.create('svg').attr('width', 500).attr('height', 100)

  let margin = { right: 50, left: 50 }
  let width = +svg.attr('width') - margin.left - margin.right
  let height = +svg.attr('height')

  let x = d3.scaleLinear()
    .domain([0, 360]) // TODO params
    .range([0, width])
    .clamp(true)

  let slider = svg.append('g')
    .attr('class', 'slider')
    .attr('transform', 'translate(' + margin.left + ',' + height / 2 + ')')

  slider.append('line')
    .attr('class', 'track')
    .attr('x1', x.range()[0] as number)
    .attr('x2', x.range()[1] as number)

    //@ts-ignore
    .select(function () { return this.parentNode.appendChild(this.cloneNode(true)) })
    .attr('class', 'track-inset')
    //@ts-ignore

    .select(function () { return this.parentNode.appendChild(this.cloneNode(true)) })
    .attr('class', 'track-overlay')
    //@ts-ignore

    .call(d3.drag()
      .on('start.interrupt', function () { slider.interrupt() })
      //@ts-ignore

      .on('start drag', function (e) {
        // TODO debounce
        const val = x.invert(e.x)
        handle.attr('cx', x(val))
        callback(val)
      })
    )

  slider.insert('g', '.track-overlay')
    .attr('class', 'ticks')
    .attr('transform', 'translate(0,' + 25 + ')')
    .selectAll('text')
    .data(x.ticks())
    .enter().append('text')
    .attr('x', x)
    .attr('text-anchor', 'middle')
    .text(function (d) { return d + '°' })

  const handle = slider.insert('circle', '.track-overlay')
    .attr('class', 'handle')
    .attr('r', 10)

  return svg.node() as SVGElement
}

import * as d3 from 'd3'

// import * as R from 'ramda'
// const DOMAIN_INCREMENT = 100 / STATII.length
// const DOMAIN = R.pipe(
//   R.length,
//   R.range(0),
//   R.map(R.pipe(R.multiply(DOMAIN_INCREMENT), R.add(DOMAIN_INCREMENT))),
//   R.prepend(0)
// )(STATII)
// console.log(DOMAIN)

const STATII = ['danger', 'warn-danger', 'warn', 'warn-success', 'success']

export const createStatusBar = (): [any, (...args: any[]) => void] => {

  const height = 20
  const width = 200

  const svg = d3.create('svg')
    .attr('class', 'status-bar')
    .attr('height', height)
    .attr('width', width)

  const colorScale = d3.scaleOrdinal(d3.range(STATII.length), STATII)

  svg.append('rect')
    .attr('class', 'background-rectangle')
    .attr('rx', 10)
    .attr('ry', 10)
    .attr('height', height)
    .attr('width', width)
    .attr('x', 0)

  const status = svg.append('rect')
    .attr('height', height)
    .attr('width', 0)
    .attr('rx', 10)
    .attr('ry', 10)
    .attr('x', 0)

  const updateStatus = (prog: number): void => {
    const colorScaleValue = colorScale(Math.floor(prog / 100 * STATII.length))
    status.transition()
      .duration(750)
      .attr('class', (): string => `status-rectangle ${colorScaleValue as string}`)
      .attr('width', (): number => (prog / 100) * width)
  }

  return [svg.node(), updateStatus]

}
const path = require('path')
const HtmlWebpackPlugin = require( 'html-webpack-plugin' )
const StylelintWebpackPlugin = require( 'stylelint-webpack-plugin' )

const mode = process.env.NODE_ENV ?? 'development'
const title = process.env.CI_PROJECT_NAME ?? `d3-everforest-expo`
 
module.exports = {
    mode,
    entry: path.resolve('./src/index.ts'),
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    resolve: {
        extensions: [ '.ts', '.js'],
    },
    output: {
        filename: 'bundle.[hash].js',
        path: path.resolve('public' ),
        clean: true
    },
    plugins: [
        new StylelintWebpackPlugin(),
        new HtmlWebpackPlugin({
            title,
        }),
    ]
}
